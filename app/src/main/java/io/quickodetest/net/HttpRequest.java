package io.quickodetest.net;

import java.io.InputStream;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class HttpRequest {

    private String url;
    private HttpRequestListener listener;

    public HttpRequest(String url, HttpRequestListener listener){
        this.url = url;
        this.listener = listener;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpRequestListener getListener() {
        return listener;
    }

    public void setListener(HttpRequestListener listener) {
        this.listener = listener;
    }

    public interface HttpRequestListener{
        public void onRequestComplete(InputStream is, String url);
        public void onRequestFailed(String error, String url);
    }
}
