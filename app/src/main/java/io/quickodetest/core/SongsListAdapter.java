package io.quickodetest.core;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import io.quickodetest.R;
import io.quickodetest.ui.NetImage;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class SongsListAdapter extends RecyclerView.Adapter<SongsListAdapter.ViewHolder> {

    private List<SongItem> items;
    private Context context;

    public SongsListAdapter(Context context, List<SongItem> items){
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_row_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTitle.setText( items.get( position ).getTitle());
        holder.txtAlbumId.setText( String.format(context.getString(R.string.album_id), items.get( position ).getAlbumId()));
        holder.txtImageId.setText( String.format(context.getString(R.string.image_id), items.get( position ).getId()));

        holder.imgImage.setImageUrl(items.get( position ).getThumbnailUrl());
        holder.imgImage.startImageDownload();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public TextView txtAlbumId;
        public TextView txtImageId;
        public NetImage imgImage;


        public ViewHolder(View view) {
            super(view);
            txtTitle    = (TextView) view.findViewById(R.id.txt_title);
            txtAlbumId  = (TextView) view.findViewById(R.id.txt_album_id);
            txtImageId  = (TextView) view.findViewById(R.id.txt_image_id);
            imgImage    = (NetImage) view.findViewById(R.id.image);
        }
    }
}
