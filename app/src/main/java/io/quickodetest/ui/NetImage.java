package io.quickodetest.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.io.InputStream;

import io.quickodetest.net.HttpRequest;
import io.quickodetest.net.Network;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class NetImage extends android.support.v7.widget.AppCompatImageView implements HttpRequest.HttpRequestListener{

    private Handler handler = new Handler();

    @Override
    public void onRequestComplete(InputStream is, String url) {
        Log.d("QK_TEST","NetImage.onRequestComplete()-> Image download complete: "+ url);
        if(url.equals(this.url))                //I know I should make a proper task cancel in real project instead of throwing out old requests result
            handleImageReceived(is);
    }


    @Override
    public void onRequestFailed(String error, String url) {
        Log.e("QK_TEST","NetImage.onRequestFailed()->Image download failed: "+ url);
    }

    private String url;

    public NetImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NetImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void setImageUrl(String url){
        this.url = url;
        setVisibility(View.INVISIBLE);
        startImageDownload();
    }

    public void startImageDownload(){
        if(TextUtils.isEmpty(url)) return;

        HttpRequest request = new HttpRequest(url, this);
        Network.addRequest(request);
    }

    private void handleImageReceived(InputStream is) {
        final Bitmap bitmap = BitmapFactory.decodeStream(is);
        handler.post(new Runnable() {
            @Override
            public void run() {
                setImageBitmap(bitmap);
                setVisibility(View.VISIBLE);
            }
        });
    }
}
