package io.quickodetest.net;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class Network {

    public static Download.Task addRequest(HttpRequest request) {
        Download.Task mCurrentTask = new Download.Task(request);
        mCurrentTask.execute();
        return mCurrentTask;
    }
}
