package io.quickodetest.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

import java.io.InputStream;
import java.util.List;

import io.quickodetest.R;
import io.quickodetest.core.SongItem;
import io.quickodetest.net.Download;
import io.quickodetest.net.HttpRequest;
import io.quickodetest.net.Network;
import io.quickodetest.parsers.SongListParser;

public class MainActivity extends AppCompatActivity {

    private SongsListFragment listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment = new SongsListFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_container, listFragment).commit();

        redownloadCatalog();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void redownloadCatalog(){
        String url = getString(R.string.catalog_url);
        HttpRequest request = new HttpRequest(url, new HttpRequest.HttpRequestListener() {
            @Override
            public void onRequestComplete(InputStream is, String url) {
                Log.d("QK_TEST","MainActivity.onRequestComplete()-> " +url);
                handleCatalogUpdate(is);
            }

            @Override
            public void onRequestFailed(String error, String url) {
                Log.e("QK_TEST","MainActivity.onRequestFailed()-> " + error);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listFragment.onDataDownloadFail(MainActivity.this);
                    }
                });

            }
        });

        Network.addRequest(request);
    }

    private void handleCatalogUpdate(InputStream is){
        final List<SongItem> items = SongListParser.parseItems(is);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listFragment.setData(items);
            }
        });
        Log.d("QK_TEST","MainActivity.handleCatalogUpdate()-> Number of items: "+ items.size());
    }

    public void refreshRequested() {
        redownloadCatalog();
    }
}
