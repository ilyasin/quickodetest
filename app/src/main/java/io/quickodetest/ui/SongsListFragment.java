package io.quickodetest.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.quickodetest.R;
import io.quickodetest.core.SongItem;
import io.quickodetest.core.SongsListAdapter;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class SongsListFragment extends Fragment {
    private Context context;

    private RecyclerView listView;
    private SongsListAdapter listAdapter;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeLayout;
    private TextView txtEmptyMessage;
    private String message = null;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.songs_list_fragment, container, false);

        txtEmptyMessage = (TextView)view.findViewById(R.id.txt_empty);
        if(!TextUtils.isEmpty(message))
            txtEmptyMessage.setText(message);
        else
            txtEmptyMessage.setText(R.string.wait_message);

        listView = (RecyclerView) view.findViewById(R.id.songs_list);
        listView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        listView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listView.getContext(),
                layoutManager.getOrientation());
        listView.addItemDecoration(dividerItemDecoration);

        swipeLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_container);
        swipeLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.BLUE, Color.GREEN);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("QK_TEST","SongsListFragment.onRefresh()-> Data refresh requested");
                MainActivity activity = (MainActivity) getActivity();
                if(activity == null){
                    Log.e("QK_TEST","Not attached to activity");
                    return;
                }
                activity.refreshRequested();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    public void onDataDownloadFail(Context context){
        if(txtEmptyMessage != null)
            txtEmptyMessage.setText(R.string.fail_message);
        else
            message = context.getString(R.string.fail_message);
    }

    public void setData(List<SongItem> items){
        listAdapter = new SongsListAdapter(context, items);
        listView.setAdapter(listAdapter);
        swipeLayout.setVisibility(View.VISIBLE);
        swipeLayout.setRefreshing(false);
    }
}
