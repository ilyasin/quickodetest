package io.quickodetest.parsers;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import io.quickodetest.core.SongItem;

/**
 * Created by ilyasin on 18-Jul-17.
 */

public class SongListParser {

    public static List<SongItem> parseItems(InputStream input){
        List<SongItem> songItems = new ArrayList<SongItem>();
        JsonReader reader = null;
        try {
            reader = new JsonReader(new InputStreamReader(input, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                songItems.add(readSongItem(reader));
            }
            reader.endArray();


        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return songItems;
    }

    private static SongItem readSongItem(JsonReader reader) throws IOException {
        SongItem item = new SongItem();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                item.setId(reader.nextString());
            } else if (name.equals("title")) {
                item.setTitle(reader.nextString());
            } else if (name.equals("albumId")) {
                item.setAlbumId(reader.nextString());
            } else if (name.equals("url")) {
                item.setUrl(reader.nextString());
            } else if (name.equals("thumbnailUrl")) {
                item.setThumbnailUrl(reader.nextString());
            }else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return item;
    }
}
