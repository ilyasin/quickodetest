package io.quickodetest.net;

/**
 * Created by ilyasin on 18-Jul-17.
 */

import android.os.Process;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;




public class Download {

    private static final String TAG = "QK_TEST";

    private static final int CORE_POOL_SIZE = 5;
    private static final int MAXIMUM_POOL_SIZE = 20;
    private static final int KEEP_ALIVE = 5;
    private static final int TASK_QUEUE_SIZE = 100;
    private static final int CONNECTION_GENERAL_TIMEOUT_MILLIS = 5000;

    private static final BlockingQueue<Runnable> sWorkQueue = new LinkedBlockingQueue<Runnable>(TASK_QUEUE_SIZE);

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {

        public Thread newThread(Runnable r) {
            return new Thread(r);
        }
    };

    private static final ThreadPoolExecutor taskExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
            TimeUnit.SECONDS, sWorkQueue, sThreadFactory);


    public static class Task {

        private final Callable<Object> worker;
        private final FutureTask<Object> future;
        private HttpRequest httpRequest;

        public Task(HttpRequest httpRequest) {
            this.httpRequest = httpRequest;

            worker = new Callable<Object>() {
                public Object call() throws Exception {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
                    return doInBackground();
                }
            };

            future = new FutureTask<Object>(worker) {
                @Override
                protected void done() {
                    Object result = null;

                    try {
                        result = get();
                    } catch (InterruptedException e) {
                        Log.d(TAG, e.toString());
                    } catch (ExecutionException e) {
                        Log.e(TAG, "An error occured while executing doInBackground()", e.getCause());
                    } catch (Throwable t) {
                        throw new RuntimeException("An error occured while executing " + "doInBackground()", t);
                    }
                }
            };
        }
        protected Object doInBackground(){
            return doInBackground_android();

        }

        protected Object doInBackground_android() {

            String requestURl = httpRequest.getUrl();

            Log.d(TAG, "Request URL: "+ requestURl);

            InputStream instream = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL(requestURl);

                conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(CONNECTION_GENERAL_TIMEOUT_MILLIS);
                conn.setReadTimeout(CONNECTION_GENERAL_TIMEOUT_MILLIS);

                int responseCode = conn.getResponseCode();
                if (responseCode / 100 != 2) {
                    httpRequest.getListener().onRequestFailed("Invalid response code: "+ responseCode, httpRequest.getUrl());
                } else {
                    instream = conn.getInputStream();
                    httpRequest.getListener().onRequestComplete(instream, httpRequest.getUrl());
                }

            } catch (Exception t) {
                httpRequest.getListener().onRequestFailed("Exception in download: " + t.toString(), httpRequest.getUrl());
                t.printStackTrace();

            }finally{
                if(instream != null)
                    try {
                        instream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                if(conn != null){
                    conn.disconnect();
                }
            }
            return null;
        }

        public final boolean cancel(boolean mayInterruptIfRunning) {
            return future.cancel(mayInterruptIfRunning);
        }

        public final Task execute() {
            try {
                taskExecutor.execute(future);
                return this;
            } catch (Exception e) {
                Log.e(TAG, "Exception on execute: " + e.toString());
                if (httpRequest != null)
                    httpRequest.getListener().onRequestFailed("Queue is full", httpRequest.getUrl());
            }
            return null;
        }
    }

}

